"use strict";

const csv = require("csvtojson");
const path = require("path");

const csvPath = path.resolve(__dirname, "..", "stores.csv");

const headers = [
  "country",
  "licenseNumber",
  "operationType",
  "establishmentType",
  "entityName",
  "dbaName",
  "streetNumber",
  "streetName",
  "line2",
  "line3",
  "city",
  "state",
  "zipCode",
  "squareFootage",
  "location"
];

let stores = [];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await csv({
      trim: true,
      headers
    })
      .fromFile(csvPath)
      .then(obj => {
        obj.forEach((item, index) => {
          let store = {
            licenseNumber: item.licenseNumber,
            operationType: item.operationType,
            establishmentType: item.establishmentType,
            entityName: item.entityName,
            dbaName: item.dbaName,
            createdAt: new Date(),
            updatedAt: new Date()
          };

          stores.push(store);
        });
      });

    return queryInterface.bulkInsert("Stores", stores, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Stores", null, {});
  }
};
