"use strict";

const csv = require("csvtojson");
const path = require("path");

const csvPath = path.resolve(__dirname, "..", "stores.csv");

const headers = [
  "country",
  "licenseNumber",
  "operationType",
  "establishmentType",
  "entityName",
  "dbaName",
  "streetNumber",
  "streetName",
  "line2",
  "line3",
  "city",
  "state",
  "zipCode",
  "squareFootage",
  "location"
];

let locations = [];

const getLongitude = location => {
  if (location && location.search("longitude") >= 0) {
    let longitude = location.match(/'longitude': '.*?'/g);
    return parseFloat(longitude[0].replace(/(?:[ 'longitude:\\\]\[]*)/g, ""));
  }
  return null;
};

const getLatitude = location => {
  if (location && location.search("latitude") >= 0) {
    let latitude = location.match(/'latitude': '.*?'/g);
    return parseFloat(latitude[0].replace(/(?:[ 'latiude:\\\]\[]*)/g, ""));
  }
  return null;
};

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await csv({
      trim: true,
      headers
    })
      .fromFile(csvPath)
      .then(obj => {
        obj.forEach((item, index) => {
          let location = {
            streetNumber: item.streetName,
            streetName: item.streetName,
            line2: item.line2,
            line3: item.line3,
            county: item.county,
            city: item.city,
            state: item.state,
            zipCode: item.zipCode,
            squareFootage: item.squareFootage,
            latitude: getLatitude(item.location),
            longitude: getLongitude(item.location),
            createdAt: new Date(),
            updatedAt: new Date(),
            storeId: index + 1
          };

          locations.push(location);
        });
      });
    return queryInterface.bulkInsert("Locations", locations, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Locations", null, {});
  }
};
