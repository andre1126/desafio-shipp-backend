# Desafio Shipp Backend

### Vitrine de Lojas

Para preparar e inicializar o server rode o comando `yarn start`.

Utilize a rota GET: /V1/stores

A requisição deve incluir os parâmetros lat(latitude) e lng(longitude). Caso contrário será retornado erro 404.
