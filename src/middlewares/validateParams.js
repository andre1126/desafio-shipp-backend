const db = require("../../models");

const validateParams = async (req, res, next) => {
  if (!req.query.lat || !req.query.lng) {
    await db.Log.create({
      latitude: req.query.lat,
      longitude: req.query.lng,
      statusCode: 404,
      storeCount: 0
    });
    res.status(404).json({ error: "Latitude and longitude params not found." });
  } else {
    next();
  }
};

module.exports = validateParams;
