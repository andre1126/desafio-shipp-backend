const db = require("../../models");

const saveLog = async (req, res) => {
  await db.Log.create({
    latitude: req.query.lat,
    longitude: req.query.lng,
    statusCode: res.statusCode,
    storeCount: res.locals.storeCount
  });
};

module.exports = saveLog;
