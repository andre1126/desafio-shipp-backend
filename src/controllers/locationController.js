const db = require("../../models");
const haversine = require("haversine");

const getLocationWithStores = async (req, res, next) => {
  try {
    let latitude = parseFloat(req.query.lat);
    let longitude = parseFloat(req.query.lng);
    const myLocation = { latitude, longitude };
    let response = [];

    let locations = await db.Location.findAll({
      include: [{ model: db.Store }]
    });

    await locations.forEach(item => {
      let distance = 0;

      if (item.latitude && item.longitude) {
        const storeLocation = {
          latitude: item.latitude,
          longitude: item.longitude
        };
        distance = haversine(myLocation, storeLocation);
      }

      location = item.toJSON();

      if (distance > 0 && distance <= 6.5) {
        response.push({ distance: distance.toFixed(2), ...location });
      }
    });

    response = response.sort(function(a, b) {
      return a.distance - b.distance;
    });

    res.locals.storeCount = response.length;
    res.json(response);
    next();
  } catch (err) {
    res.locals.storeCount = 0;
    res
      .status(500)
      .json({
        error: "An error occurred while querying locations and stores."
      });
    next();
  }
};

module.exports = getLocationWithStores;
