const getLocationWithStores = require("./controllers/locationController");
const validateParams = require("./middlewares/validateParams");
const saveLog = require("./middlewares/saveLog");

const express = require("express");
const router = express.Router();

router.get("/V1/stores", validateParams, getLocationWithStores);
router.use(saveLog);

module.exports = router;
