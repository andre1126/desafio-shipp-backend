"use strict";
module.exports = (sequelize, DataTypes) => {
  const Store = sequelize.define(
    "Store",
    {
      licenseNumber: DataTypes.STRING,
      operationType: DataTypes.STRING,
      establishmentType: DataTypes.STRING,
      entityName: DataTypes.STRING,
      dbaName: DataTypes.STRING
    },
    {}
  );
  Store.associate = function(models) {
    // associations can be defined here
  };
  return Store;
};
