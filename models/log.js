"use strict";
module.exports = (sequelize, DataTypes) => {
  const Log = sequelize.define(
    "Log",
    {
      latitude: DataTypes.DOUBLE,
      longitude: DataTypes.DOUBLE,
      statusCode: DataTypes.INTEGER,
      storeCount: DataTypes.INTEGER
    },
    {}
  );
  Log.associate = function(models) {
    // associations can be defined here
  };
  return Log;
};
