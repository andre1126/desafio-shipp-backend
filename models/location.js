"use strict";
module.exports = (sequelize, DataTypes) => {
  const Location = sequelize.define(
    "Location",
    {
      streetNumber: DataTypes.STRING,
      streetName: DataTypes.STRING,
      line2: DataTypes.STRING,
      line3: DataTypes.STRING,
      county: DataTypes.STRING,
      city: DataTypes.STRING,
      state: DataTypes.STRING,
      zipCode: DataTypes.STRING,
      squareFootage: DataTypes.DOUBLE,
      latitude: DataTypes.DOUBLE,
      longitude: DataTypes.DOUBLE
    },
    {}
  );
  Location.associate = function(models) {
    Location.belongsTo(models.Store);
  };
  return Location;
};
